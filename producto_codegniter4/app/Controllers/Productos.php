<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Entities\User;
use App\Models\Producto;

class Productos extends Controller
{
	public function index(){
		$producto = new Producto();

		$data['producto'] = $producto->getProducto();
		$data['proveedor'] = $producto->getProveedor();
		$data['marca'] = $producto->getMarca();
		$data['categoria'] = $producto->getCategoria();
		echo view('templates/header', $data);
		echo view('producto/overview');
		echo view('templates/footer');

	   }

	   public function delete($id){
		$producto = new Producto();

		$msj = $producto->eliminar($id);

		if ($msj == true) {
			echo "<script>alert('Eliminado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Eliminar!!!')</script>";
		}

		return redirect()->to('/producto_codegniter4/public/Productos');
	}

	public function insertar()
	{
		$producto = new Producto();

		// Create
		$user = new User();
		$data = [
			'nombre_producto'=> $this->request->getPost('nombre_producto'),
			'precio'=> md5($this->request->getPost('precio')),
			'stock'=>$this->request->getPost('stock'),
			'id_proveedor'=>$this->request->getPost('proveedor'),
			'id_marca'=>$this->request->getPost('marca'),
			'id_categoria'=>$this->request->getPost('categoria')

		]; 

		$user->fill($data);

		if ($producto->save($user) == true) {
			echo "<script>alert('Agregado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Agregar!!!')</script>";
		}

		return redirect()->to('/producto_codegniter4/public/Productos');
	}





	//-------------------------------------------------------------------
	//--------------------------------------------------------------------	
}