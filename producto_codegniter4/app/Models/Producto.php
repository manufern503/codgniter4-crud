<?php 

namespace App\Models;
use CodeIgniter\Model;

class Producto extends Model{

    protected $table      = 'producto';
    protected $primaryKey = 'id_producto';


    protected $returnType = 'App\Entities\User';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['id_producto', 'nombre_producto', 'precio', 'stock', 'id_proveedor', 'id_marca','id_categoria'];

public function getProducto(){

    	$pa_consultar = 'Call pa_consultar()';
		$query = $this->query($pa_consultar);
		return $query->getResult();
	     }

	     public function eliminar($id)
	{
		$pa_eliminar = 'CALL pa_eliminar(?)';
		$query = $this->query($pa_eliminar, [$id]);

		if ($query) {
			return true;
		}else{
			return false;
		}
	}

	public function getProveedor()
	{
		$getProveedores = 'CALL getProveedores()';
		$query = $this->query($getProveedores);

		return $query->getResult();
	}
	public function getMarca()
	{
		$getMarcas = 'CALL getMarcas()';
		$query = $this->query($getMarcas);

		return $query->getResult();
	}
	public function getCategoria()
	{
		$getCategorias = 'CALL getCategorias()';
		$query = $this->query($getCategorias);

		return $query->getResult();
	}

	
}

